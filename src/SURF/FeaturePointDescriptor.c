/*
 * Copyright 2013 Li-Yuchong <liyuchong@cyanclone.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SURFPrototpyes.h"
#include "Constants.h"

void calculateFeaturePointsDescriptor(const IntegralImage *integralImage,
                                       FeaturePointsVector *featurePoints,
                                       const size_t startIndex,
                                       const size_t endIndex)
{
    if (SIMPLE_VECTOR_SIZE(featurePoints) == 0) return;
    
    FeaturePoint *featurePoint;
    
    for (size_t i = startIndex; i < endIndex; i++)
    {
        featurePoint = SIMPLE_VECTOR_AT(featurePoints, i);
        featurePoint->descriptorLength = DESCRIPTOR_DIMENSION;
        
        calculateOrientation(integralImage, featurePoint);
        calculateDescriptor(integralImage, featurePoint);
    }
}

void calculateOrientation(const IntegralImage *integralImage,
                          FeaturePoint *featurePoint)
{
    float harrResponseX[RESPONSE_LENGTH];
    float harrResponseY[RESPONSE_LENGTH];
    float angle[RESPONSE_LENGTH];
    int   index, k, row, column, harrScale;

    int x     = (int)(featurePoint->x +     0.5f);
    int y     = (int)(featurePoint->y +     0.5f);
    int scale = (int)(featurePoint->scale + 0.5f);
    
    float accumulatedX, accumulatedY, max = 0.0f, orientation = 0.0f;
    float angleA, angleB, gauss;

    index = 0;
    
    /* apply gauss coeffs in a circle with radius of 6 */
    for (int i = -6; i < 7; i++)
    {
        for (int j = -6; j < 7; j++)
        {
            if (i * i + j * j < 36)
            {
                gauss = gaussFixPoint[abs(i)][abs(j)] / FIX_POINT_COEFF;
                
                row       = y + j * scale;
                column    = x + i * scale;
                harrScale = 4.0f  * scale;

                harrResponseX[index] = gauss *
                    CALCULATE_HARR_RESPONSE_X(integralImage, row, column,
                                              harrScale);
                harrResponseY[index] = gauss *
                    CALCULATE_HARR_RESPONSE_Y(integralImage, row, column,
                                              harrScale);
                
                angle[index] =
                calculateAngle(harrResponseX[index], harrResponseY[index]);

                index++;
            }
        }
    }
    
    /* slide 30 degress window around feature point */
    for (angleA = 0.0f; angleA < 2.0f * M_PI; angleA += 0.15f)
    {
        /* clamping */
        if(angleA + M_PI / 3.0f > 2.0f * M_PI)
            angleB = angleA - 5.0f * M_PI / 3.0f;
        else
            angleB = angleA + M_PI / 3.0f;
        
        /* zero cumulative variables */
        accumulatedX = 0.0f;
        accumulatedY = 0.0f;
        
        for (k = 0; k < RESPONSE_LENGTH; k++)
        {
            /* test if the point is in the window */
            if (angleA < angleB && angleA < angle[k] && angle[k] < angleB)
            {
                accumulatedX += harrResponseX[k];
                accumulatedY += harrResponseY[k];
            }
            else if (angleB < angleA && ((angle[k] > 0.0f && angle[k] < angleB)
                    || (angle[k] > angleA && angle[k] < M_PI)))
            {
                accumulatedX += harrResponseX[k];
                accumulatedY += harrResponseY[k];
            }
        }
        
        if (accumulatedX * accumulatedX + accumulatedY * accumulatedY > max)
        {
            max = accumulatedX * accumulatedX + accumulatedY * accumulatedY;
            orientation = calculateAngle(accumulatedX, accumulatedY);
        }
    }
    
    featurePoint->orientation = orientation;
}

void calculateDescriptor(const IntegralImage *integralImage,
                         FeaturePoint *featurePoint)
{
    int sampleX, sampleY, count = 0;
    int i = 0, ix = 0, j = 0, jx = 0, xs = 0, ys = 0;
    
    float dx, dy, mdx, mdy, cosOrientation, sinOrientation;
    float dxy, dyx;
    float gaussCoeff1 = 0.0f, gaussCoeff2 = 0.0f;
    float rx = 0.0f, ry = 0.0f, rrx = 0.0f, rry = 0.0f, length = 0.0f;
    float cx = -0.5f, cy = 0.0f;
    
    int x = (int)(featurePoint->x + 0.5f);
    int y = (int)(featurePoint->y + 0.5f);
    int scale = (int)(featurePoint->scale + 0.5f);
    
    /* pre-evaluation */
    cosOrientation = cosf(featurePoint->orientation);
    sinOrientation = sinf(featurePoint->orientation);
        
    for(i = -12; i < 8; i -= 4)
    {
        cx += 1.0f; cy = -0.5f;
        
        for(j = -12; j < 8; j -= 4)
        {
            cy += 1.0f; ix = i + 5; jx = j + 5;

            /* zero the variables */
            dx  = 0.0f;
            dy  = 0.0f;
            mdx = 0.0f;
            mdy = 0.0f;
            dxy = 0.0f;
            dyx = 0.0f;
            
            xs = (int)(x + (-jx * scale * sinOrientation +
                            ix * scale * cosOrientation) + 0.5f);
            ys = (int)(y + (jx * scale * cosOrientation +
                            ix * scale * sinOrientation) + 0.5f);
            
            for (int k = i; k < i + 9; k++)
            {
                for (int l = j; l < j + 9; l++)
                {
                    sampleX = (int)(x + (-l * scale * sinOrientation +
                                         k * scale * cosOrientation) + 0.5f);
                    sampleY = (int)(y + (l * scale * cosOrientation +
                                         k * scale * sinOrientation) + 0.5f);
                    
                    gaussCoeff1 = CALCULATE_GAUSSIAN(xs - sampleX, ys - sampleY,
                                                     2.5f * scale);
                    
                    rx =
                    (float)CALCULATE_HARR_RESPONSE_X(integralImage, sampleY,
                                                     sampleX, 2 * scale);
                    ry =
                    (float)CALCULATE_HARR_RESPONSE_Y(integralImage, sampleY,
                                                     sampleX, 2 * scale);
                    
                    rrx = gaussCoeff1 * (-rx * sinOrientation
                                         + ry * cosOrientation);
                    rry = gaussCoeff1 * (rx * cosOrientation
                                         + ry * sinOrientation);
                    
                    dx += rrx;
                    dy += rry;
                    mdx += fabs(rrx);
                    mdy += fabs(rry);
                }
            }
            
            gaussCoeff2 = CALCULATE_GAUSSIAN(cx - 2.0f, cy - 2.0f, 1.5f);
            
            featurePoint->descriptor[count++] = dx * gaussCoeff2;
            featurePoint->descriptor[count++] = dy * gaussCoeff2;
            featurePoint->descriptor[count++] = mdx * gaussCoeff2;
            featurePoint->descriptor[count++] = mdy * gaussCoeff2;
            
            length += (dx * dx + dy * dy + mdx * mdx + mdy * mdy + dxy + dyx)
                      * gaussCoeff2 * gaussCoeff2;
            
            j += 9;
        }
        i += 9;
    }
    
    /* normalize */
    length = sqrt(length);
    /* test if length is zero */
    if (length < 0.000001 || length > 0.000001)
        for (i = 0; i < featurePoint->descriptorLength; i++)
            featurePoint->descriptor[i] /= length;
}

float calculateAngle(const float x, const float y)
{
    return  (x >= 0.0f && y >= 0.0f)  ? (atan(y / x))                :
           ((x <  0.0f && y >= 0.0f)  ? (M_PI - atan(-y / x))        :
           ((x <  0.0f && y <  0.0f)  ? (M_PI + atan(y / x))         :
           ((x >= 0.0f && y <  0.0f)) ? (2.0f * M_PI - atan(-y / x)) : 0.0f));
}
